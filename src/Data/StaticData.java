package Data;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import Models.Address;
import Models.Author;
import Models.BookCopy;
import Models.BookInfo;
import Models.CheckoutRecord;
import Models.User;

 public class StaticData {
 public	static ArrayList<User> users = new ArrayList<User>();
 public	static ArrayList<BookInfo> Books = new ArrayList<BookInfo>();
 public	static ArrayList<Author> authors= new ArrayList<Author>();
 public	static ArrayList<CheckoutRecord> CheckOutRecords = new ArrayList<CheckoutRecord>();



private static void InitUsersList()
{
	User u1=new User("1","1","Youssef","louka");
	u1.addRole(User.roleType.ADMIN);
	u1.addRole(User.roleType.LIBRARIAN);
	u1.addAddress(new Address("USA","New York","123"));
	u1.addAddress(new Address("Egypt","Cairo","897"));

	User u2=new User("2","2","Alaa","Hassanein");
	u2.addRole(User.roleType.LIBRARIAN);
	u2.addAddress(new Address("Fair Field","MUM","23233"));
	u2.addAddress(new Address("CALIFORNIA","wert","768 5"));

	User u3=new User("3","3","Joe","Hany");
	u2.addRole(User.roleType.MEMBER);
	u2.addAddress(new Address("London","Giza","3244324"));
	u2.addAddress(new Address("France","9","326 5"));
	
	users.add(u1);
	users.add(u2);
	users.add(u3);
}

private static void AddBooksToAuthors()
 {
	authors.get(0).AddBookInfo(Books.get(0));
	authors.get(1).AddBookInfo(Books.get(1));
 }
 private static void InitAuthors()
 {
	    Author a1=new Author();
	 	a1.setId("1001");
	 	a1.setFirstName("Jon");
	 	a1.setLastName("Tom");
	 //	a1.AddBookInfo(Books.get(0));

	 	Author a2=new Author();
	 	a2.setId("1007");
	 	a2.setFirstName("Kamma");
	 	a2.setLastName("Martin");
	//	a1.AddBookInfo(Books.get(1));
		authors.add(a1);
	 	authors.add(a2);
 }
 
 public static String PrintBookInfo(BookInfo newBook)
	{
		String str="";
		 str+="   --New Book Information--"+"\n";
		 str+="ISBN: "+newBook.getISBN()+"\n";
		 str+="Title: "+newBook.getTitle()+"\n";
		 
		
		 
		 Author A=newBook.getAuthors().get(0);
		
		 str+="Authors: "+A.getFirstName()+" "+A.getLastName()+" ID:"+A.getId()+"\n";
		 str+="No of Copies:"+"\n";
		
		 
		 int count=0;
		 for(BookCopy bc:newBook.getBookCopies())
		 {
			 count++;
			
			 str+="No "+count+"  IsAvailable:"+bc.GetIsAvailable()+"   ISBN:"+bc.GetBookInfo().getISBN()+"  Title:"+bc.GetBookInfo().getTitle()+"\n";
		 }
		 return str;
	}

 private static void InitBooksList()
 {
 	BookInfo b1=new BookInfo("1111","How To Proogram",new ArrayList<Author>(),new ArrayList<BookCopy>());
 	

 	b1.addAuthor(authors.get(0));

 	b1.addBookCopy(new BookCopy(b1));
 	b1.addBookCopy(new BookCopy(b1));

 	BookInfo b2=new BookInfo("2222","Complete Reference",new ArrayList<Author>(),new ArrayList<BookCopy>());


 	b2.addAuthor(authors.get(1));

	b2.addBookCopy(new BookCopy(b2));
	b2.addBookCopy(new BookCopy(b2));
	b2.addBookCopy(new BookCopy(b2));
	b2.addBookCopy(new BookCopy(b2));


	Books.add(b1);
	Books.add(b2);
 }

 private static void InitCheckOutRecords()
 {
	 CheckoutRecord r1=new CheckoutRecord();
	 r1.setMember(users.get(0));

	 BookInfo book=Books.get(0);
	 System.out.println(">>>>>>" + book.GetAvailableBookCopy());
	 BookCopy bCopy=book.GetAvailableBookCopy();
	 r1.setBookCopy(bCopy);
	
	
	Calendar cal = Calendar.getInstance();
	Date checkoutDate = cal.getTime();
	cal.add(Calendar.MONTH, 1);
	Date dueDate = cal.getTime();
	
	r1.setCheckoutDate(checkoutDate);
	r1.setDueDate(dueDate);
	users.get(0).addRecord(r1);


	 CheckoutRecord r2=new CheckoutRecord();
	 r2.setMember(users.get(1));

	 BookInfo book2=Books.get(1);
	 BookCopy bCopy2=book.GetAvailableBookCopy();

	 r2.setBookCopy(bCopy2);
	r2.setCheckoutDate(checkoutDate);
	r2.setDueDate(dueDate);
	users.get(1).addRecord(r2);
 }


 public static void initData()
 {
	 InitUsersList();
	 InitAuthors();
	 InitBooksList();
	 AddBooksToAuthors();
	 InitCheckOutRecords();

 }


}
