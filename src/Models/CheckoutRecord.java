package Models;

import java.text.SimpleDateFormat;
import java.util.Date;

public class CheckoutRecord {
    private User member;

    private Date dueDate;

    private Date checkoutDate;

    private BookCopy bookCopy;

	private SimpleDateFormat format = new SimpleDateFormat("MM-dd-yyyy");

    public User getMember() {
        return this.member;
    }

    public void setMember(User user) {
        this.member = user;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    public Date getDueDate() {
        return this.dueDate;
    }

    public void setCheckoutDate(Date checkoutDate) {
        this.checkoutDate = checkoutDate;
    }

    public Date getCheckoutDate() {
        return this.checkoutDate;
    }

    public void setBookCopy(BookCopy bookCopy) {
        this.bookCopy = bookCopy;
    }
    
    public String getBookName() {
    	return bookCopy.GetBookInfo().getTitle();
    }
    
    public String getDueDateFormatted() {
    	return format.format(this.dueDate);
    }
    
    public String getCheckoutDateFormatted() {
    	return format.format(this.checkoutDate);
    }
}
