package Models;

public class BookCopy {
	private BookInfo book;
	private boolean IsAvailable;
	
	public BookCopy(BookInfo book) {
		this.book = book;
		this.IsAvailable = true;
	}
	
	public BookInfo GetBookInfo()
	{
	  return	book;
	}
	
	public boolean GetIsAvailable()
	{
	  return	IsAvailable;
	}
	
	public void SetIsAvailable(boolean _isAvailable)
	{
	     	this.IsAvailable=_isAvailable;
	}
}
