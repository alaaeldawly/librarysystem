package Models;

import java.util.ArrayList;

public class Author {
	private ArrayList<Address> addresses = new ArrayList<Address>();
	private ArrayList<BookInfo> books = new ArrayList<BookInfo>();
	
	private String firstName;
	private String lastName;
	private String id;
	
	public void AddBookInfo(BookInfo book) {
		books.add(book);
	}
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public String getFirstName() {
		return this.firstName;
	}
	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public String getLastName() {
		return this.lastName;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public String getId() {
		return this.id;
	}
}
