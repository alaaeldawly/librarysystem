package Models;

import java.util.ArrayList;
import java.util.List;

public class BookInfo {
	private ArrayList<BookCopy> copies = new ArrayList<BookCopy>();
	String ISBN;
	String title;
	ArrayList<Author> authors = new ArrayList<Author>();
	ArrayList<BookCopy> bookCopies = new ArrayList<BookCopy>();

	public BookInfo(Author author) {
		this.authors.add(author);
	}
	
	public BookCopy GetAvailableBookCopy() {
		for(BookCopy bc:bookCopies)
		{
			if(bc.GetIsAvailable())
				return bc;
		}
		return null;
	}
	
	// Alaa
	public BookInfo(String _isbn,String _title, ArrayList<Author> _authors,ArrayList<BookCopy> _bookCopies) {
		this.ISBN=_isbn;
		this.title=_title;
		this.authors=_authors;
		this.bookCopies=_bookCopies;
	}

	public void setISBN(String isbn) {
		ISBN = isbn;
	}

	public String getISBN() {
		return this.ISBN;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getTitle() {
		return this.title;
	}

	public void addAuthor(Author author) {
		this.authors.add(author);
	}

	public ArrayList<Author> getAuthors() {
		return this.authors;
	}

	public void addBookCopy(BookCopy bookCopy) {
		this.bookCopies.add(bookCopy);
	}

	public ArrayList<BookCopy> getBookCopies() {
		return this.bookCopies;
	}
	public void ResetBookCopies() {
		 this.bookCopies=null;
	}
}
