package Models;

import java.util.ArrayList;

public class User {
	private ArrayList<Address> addresses = new ArrayList<Address>();
	
	public enum roleType {
		ADMIN,
		LIBRARIAN,
		MEMBER
	}
	
	private ArrayList<CheckoutRecord> checkoutRecords = new ArrayList<CheckoutRecord>();
	
	ArrayList<roleType> roles = new ArrayList<roleType>();

	private String id;

	private String password;

	private String firstName;

	private String lastName;
	
	public void addRole(roleType role) {
		roles.add(role);
	}
	
	public ArrayList<roleType> getRoles() {
		return this.roles;
	}
	
	public void addRecord(CheckoutRecord rec) {
		this.checkoutRecords.add(rec);
	}
	
	public ArrayList<CheckoutRecord> getRecords() {
		return this.checkoutRecords;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void addAddress(Address address) {
		addresses.add(address);
	}

	public ArrayList<Address> getAddresses() {
		return this.addresses;
	}
	
	public User () {
		
	}
	
	public User(String id,String password,String firstName,String lastName)
	{
		this.id=id;
		this.password=password;
		this.lastName=lastName;
		this.firstName=firstName;;
	}

	public boolean isMember() {
		return this.roles.contains(roleType.MEMBER);
	}

	public boolean isAdmin() {
		return this.roles.contains(roleType.ADMIN);
	}

	public boolean isLibrarian() {
		return this.roles.contains(roleType.LIBRARIAN);
	}
}
