package Controllers;
import java.util.ArrayList;

import Data.StaticData;
import Models.Author;
import Models.BookCopy;

import Models.BookInfo;

public class BookInfoCtrl {

	private BookInfoCtrl() {};
	
public	static BookInfo FindBookByISBN(String isbn)
	{
		for(BookInfo b: StaticData.Books)
		{
			if(b.getISBN().equals(isbn))
				return b;
		}		
		return null;
	}
	
public	static BookInfo AddBookInfo(String _isbn,String _title, ArrayList<Author> _authors,ArrayList<BookCopy> _bookCopies)
	{
		BookInfo NewBook=new BookInfo(_isbn,_title,_authors,_bookCopies);
		StaticData.Books.add(NewBook);
		return NewBook;
	}

public	static BookInfo GetBookByISBN(String isbn)
	{
		for(BookInfo b: StaticData.Books)
		{
			if(b.getISBN().equals(isbn))
				return b;
		}		
		return null;
	}
	
   public	static BookInfo UpdateBook(BookInfo _Updbook,int NoOfCopies)
	{
		//  Update BookInfo by ISBN	
		for(int i=0;i<StaticData.Books.size();i++)
		{
			if(StaticData.Books.get(i).getISBN().equals(_Updbook.getISBN()))
			{
				StaticData.Books.get(i).ResetBookCopies();
				StaticData.Books.set(i, _Updbook);
				 try {
					BookCopyCtrl.AddBookCopiesByISBN(StaticData.Books.get(i).getISBN(),NoOfCopies);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return StaticData.Books.get(i);
			}				
		}
		
		return null;
	}
	
   public	static void AssigBookToAuthor(BookInfo _book,Author au)
	{
	   for(int i=0;i<StaticData.authors.size();i++) 
		{
		   if(StaticData.authors.get(i).equals(au))
			{
			   StaticData.authors.get(i).AddBookInfo(_book);
			}
		}
	}
}
