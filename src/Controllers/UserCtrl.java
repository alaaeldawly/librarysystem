package Controllers;

import java.util.ArrayList;

import Data.StaticData;
import Models.User;

public class UserCtrl {
	private UserCtrl () {};

	public static User login(String id, String password) throws Exception {
		User user = getUserById(id);

		if(user == null) {
			throw new Exception("User not found");
		}
		if(!password.equals(user.getPassword()) || !id.equals(user.getId())) {
			throw new Exception("Invalid credentials");
		}
		if(!user.isAdmin() && !user.isLibrarian()) {
			throw new Exception("You are not authorized to login");
		}

		return user;
	}


     public static User getUserById(String userid)
      {
    	 ArrayList<User> users = StaticData.users;
    	 System.out.println(users.size());
    	 System.out.println(users.get(0));
    	 for(User u: users)
    	 {
    		 if(userid.equals(u.getId()))
    			 return u;
    	 }
    	 
    	 return null;
	  }
}
