package Controllers;

import Models.CheckoutRecord;
import Models.User;

import java.util.ArrayList;

import Data.StaticData;

public class MemberCtrl {
	private MemberCtrl () {};

	public static ArrayList<CheckoutRecord>  search(String memberId) throws Exception {

		User user = UserCtrl.getUserById(memberId);
		
		if(user == null) {
			throw new Exception("User not found");
		}
		if(!user.isMember()) {
			throw new Exception("User not a library member");
		}

		return user.getRecords();
	}

}
