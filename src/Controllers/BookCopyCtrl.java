package Controllers;

import java.util.List;

import Data.StaticData;
import Models.BookCopy;
import Models.BookInfo;

public class BookCopyCtrl {
	private BookCopyCtrl() {};
	
public	static void AddBookCopiesByISBN(String isbn,int NoOfCopies) throws Exception
	{
		// get BookInfo By ISBN		
		BookInfo bookInfoObj= BookInfoCtrl.GetBookByISBN(isbn);
		
		if(bookInfoObj == null) {
			throw new Exception("Book not found");
		}
		
		
		for(int i=0;i<StaticData.Books.size();i++)
		{
			if(StaticData.Books.get(i).getISBN().equals(bookInfoObj.getISBN()))
			{
				for(int k=0;k<NoOfCopies;k++ )
				{
					BookCopy NewBookCopy=new BookCopy(bookInfoObj);
					NewBookCopy.SetIsAvailable(true);
					StaticData.Books.get(i).addBookCopy(NewBookCopy);	
					
				}	
				return;
			}
		}	
		
	}
}
