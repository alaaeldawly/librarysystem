package Controllers;

import Data.StaticData;
import Models.User;

public class AdminCtrl {
	private AdminCtrl() {}

	
	public static void AddNewMember(User user )
	{
	  StaticData.users.add(user);		
	}
	
	static void RemoveMember(String userId )
	{
		for(int i=0;i<StaticData.users.size();i++)
		{
			if(StaticData.users.get(i).getId().equals(userId))
			{
				StaticData.users.remove(i);
				return;
			}				
		}
	}
}
