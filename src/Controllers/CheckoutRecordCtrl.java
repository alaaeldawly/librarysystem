package Controllers;

import java.util.Calendar;
import java.util.Date;

import Data.StaticData;
import Models.*;

public class CheckoutRecordCtrl {
	private CheckoutRecordCtrl () {};

	public static CheckoutRecord addCheckoutRecord(String ISBN, String userId) throws Exception {
		BookInfo book = BookInfoCtrl.FindBookByISBN(ISBN);
		if(book == null) {
			throw new Exception("Book not found");
		}

		BookCopy bookCopy = book.GetAvailableBookCopy();
		if(bookCopy == null) {
			throw new Exception("No available copies");
		}

		User member =UserCtrl.getUserById(userId);
		if(member == null) {
			throw new Exception("Member not found");
		}

		else if(!member.isMember()) {
			throw new Exception("Member not found");
		}

		CheckoutRecord cor = new CheckoutRecord();
		cor.setBookCopy(bookCopy);
		cor.setMember(member);

		// Set dates
		Calendar cal = Calendar.getInstance();
		Date checkoutDate = cal.getTime();
		cal.add(Calendar.MONTH, 1);
		Date dueDate = cal.getTime();
		cor.setCheckoutDate(checkoutDate);
		cor.setDueDate(dueDate);
		
		member.addRecord(cor);
		bookCopy.SetIsAvailable(false);
		StaticData.CheckOutRecords.add(cor);

		return cor;
	}
	
}
