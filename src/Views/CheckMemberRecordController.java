package Views;

import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.ResourceBundle;

import Controllers.MemberCtrl;
import Models.CheckoutRecord;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;

public class CheckMemberRecordController implements Initializable {
	  @FXML  TextField id;
	  @FXML  TableView<CheckoutRecord> recordsView;
	  @FXML  TableColumn<CheckoutRecord, String> columnOne;
	  @FXML  TableColumn<CheckoutRecord, String> columnTwo;
	  @FXML  TableColumn<CheckoutRecord, String> columnThree;
	  @FXML  Button check;

	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
		check.setOnAction(event -> {
			try {
				handleACheckAction(event);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		});
		columnOne.setCellValueFactory(new PropertyValueFactory<CheckoutRecord, String>("bookName"));
		columnTwo.setCellValueFactory(new PropertyValueFactory<CheckoutRecord, String>("checkoutDateFormatted"));
		columnThree.setCellValueFactory(new PropertyValueFactory<CheckoutRecord, String>("DueDateFormatted"));
	}
	
	
	private void handleACheckAction(ActionEvent event) throws IOException{

		String memberId = id.getCharacters().toString();

		try {
			ArrayList<CheckoutRecord> records = MemberCtrl.search(memberId);
			if(records.size() == 0) throw new Exception("No records found for " + memberId);
			recordsView.setItems(FXCollections.observableList(records));
		}
		catch(Exception e) {
			new Alert(Alert.AlertType.INFORMATION, e.getMessage()).showAndWait();
		}
		
	}

}
