package Views;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import Controllers.BookCopyCtrl;
import Controllers.BookInfoCtrl;
import Data.StaticData;
import Models.BookInfo;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;

public class BookCopyController implements Initializable {
	  @FXML  TextField isbn;
	  @FXML  TextField copies;
	  @FXML  Button addbtn;

	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
		addbtn.setOnAction(event -> {
			try {
				handleAddBookAction(event);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		});
		
	}
	
	
	private void handleAddBookAction(ActionEvent event) throws NumberFormatException, Exception{
		//System.out.println("book copy");
			
		
		BookInfo newBook=BookInfoCtrl.FindBookByISBN(isbn.getText());
		if(newBook==null) {
			 Alert alert = new Alert(AlertType.WARNING, "No Book found.");
			 alert.show();
		}
		else {
			BookCopyCtrl.AddBookCopiesByISBN(isbn.getText(),Integer.parseInt(copies.getText()));
			String str=StaticData.PrintBookInfo(newBook);
			
			Alert alert = new Alert(AlertType.INFORMATION, str);
			alert.show();
		}
		
	}

}
