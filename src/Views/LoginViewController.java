package Views;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import Controllers.*;
import Models.User;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class LoginViewController implements Initializable {
	String user = "pyramids";
	String pass = "123";

	  @FXML  Button loginButton;
	  @FXML  TextField username;
	  @FXML  PasswordField password;
	  @FXML  Label errorMessage;

	  public static User loggedInUser;
	  private void handleButtonAction(ActionEvent event) throws IOException{
		  errorMessage.setText("");

		  //Login Handle
		  try {
			  User user = UserCtrl.login(username.getText(), password.getText());
			  loggedInUser = user;
			  	FXMLLoader fxmlLoader = new 
				FXMLLoader(getClass().getResource("/Views/ProfileScreen.fxml"));
				Parent root1 = (Parent) fxmlLoader.load();
				Stage stage = new Stage();
				//set what you want on your stage
				stage.initModality(Modality.APPLICATION_MODAL);
				stage.setTitle("Dashboard");
				stage.setScene(new Scene(root1));
				stage.setResizable(false);
				stage.show();
		  } catch(Exception ex) {
		  	System.out.println(ex.getStackTrace().toString());
			  errorMessage.setText(ex.getMessage());
			  
		  }

	  }


	@Override
	public void initialize(URL location, ResourceBundle resources)  {
	
		// TODO Auto-generated method stub
		loginButton.setOnAction(event -> {
			try {
				handleButtonAction(event);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		});

	}
	  

}
