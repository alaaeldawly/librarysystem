package Views;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

import Controllers.BookCopyCtrl;
import Controllers.BookInfoCtrl;
import Data.StaticData;
import Models.Author;
import Models.BookCopy;
import Models.BookInfo;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;

public class BookController implements Initializable {
	  @FXML  TextField isbn;
	  @FXML  TextField name;
	  @FXML  TextField copies;
	  @FXML  ComboBox<String> author;
	  @FXML  Button addbtn;

	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
		fillComboBoxAuthors();
		addbtn.setOnAction(event -> {
			try {
				handleAddBookAction(event);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		});
		
	}
	
	
	private void handleAddBookAction(ActionEvent event) throws NumberFormatException, Exception{
		// String _isbn,String _title, ArrayList<Author> _authors,ArrayList<BookCopy> _bookCopies
		
		String authorName=author.getValue();
		Author au=new Author();
		for(Author A:StaticData.authors) 
		{
			if(A.getFirstName().equals(authorName))
				au=A;
		}
	 ArrayList<Author> authors=new ArrayList<Author>();
	 authors.add(au);

	 ArrayList<BookCopy> bookcopies=new ArrayList<BookCopy>();	
	 
	 BookInfo newBook= BookInfoCtrl.AddBookInfo(isbn.getText(),name.getText(),authors,bookcopies);
	 BookCopyCtrl.AddBookCopiesByISBN(isbn.getText(),Integer.parseInt(copies.getText()));	 
	
	 // Set NewBook for author
	 BookInfoCtrl.AssigBookToAuthor(newBook, au);
	
	 
	 
	 
	 isbn.setText("");
	 name.setText("");
	 copies.setText("");
	 author.setValue("");
	 
	 
	 String str=StaticData.PrintBookInfo(newBook);
	 

	 
	 Alert alert = new Alert(AlertType.INFORMATION, str);
	 alert.show();
	}
	
	
	
	private void fillComboBoxAuthors()
	{
		for(Author a:StaticData.authors)
		{
			author.getItems().add(a.getFirstName());
		}
		
	}
}
