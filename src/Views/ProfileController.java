package Views;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class ProfileController implements Initializable { 
	
	 @FXML  Button addMember; 
	 @FXML  Button addBook;
	 @FXML  Button addCopy;
	 @FXML  Button showCheckout;
	 @FXML  Button bookCheck;
	 @FXML  Button btneditbook;


@Override
public void initialize(URL location, ResourceBundle resources) {
	if(!LoginViewController.loggedInUser.isAdmin()) {
		addMember.setVisible(false);
		addBook.setVisible(false);
		addCopy.setVisible(false);
	}
	if(!LoginViewController.loggedInUser.isLibrarian()) {
		showCheckout.setVisible(false);
		bookCheck.setVisible(false);
	}

	// TODO Auto-generated method stub
	addMember.setOnAction(event -> {
		try {
			handleAddMemberAction(event);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	});

	addBook.setOnAction(event -> {
		try {
			handleAddBookAction(event);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	});

	addCopy.setOnAction(event -> {
		try {
			handleAddCopyAction(event);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	});

	showCheckout.setOnAction(event -> {
		try {
			handleShowCheckoutAction(event);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	});

	bookCheck.setOnAction(event -> {
		try {
			handleBookCheckAction(event);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	});

	btneditbook.setOnAction(event -> {
		try {
			handleEditBookAction(event);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	});

	
	
	
}

private void loadFxml (String title, String filename) throws IOException {
	 FXMLLoader fxmlLoader = new 
		        FXMLLoader(getClass().getResource(filename));
		    Parent root1 = (Parent) fxmlLoader.load();
		    Stage stage = new Stage();
		    //set what you want on your stage
		    stage.initModality(Modality.APPLICATION_MODAL);
		    stage.setTitle(title);
		    stage.setScene(new Scene(root1));
		    stage.setResizable(false);
		    stage.show();
}

private void handleAddMemberAction(ActionEvent event) throws IOException {
	
		    
		    
		    loadFxml("Add Member", "/Views/AddMemberController.fxml");
	  
	
}
private void handleAddBookAction(ActionEvent event) throws IOException {
	 
		    loadFxml("Add Book", "/Views/BookScreen.fxml");

	
	
}
private void handleShowCheckoutAction(ActionEvent event)  throws IOException{
	
    loadFxml("Show Records", "/Views/CheckMemberRecord.fxml");

	
}
private void handleAddCopyAction(ActionEvent event)throws IOException {
	
    loadFxml("Add Book Copy", "/Views/BookCopyScreen.fxml");

}
private void handleBookCheckAction(ActionEvent event) throws IOException{
	
    loadFxml("CheckOut Book", "/Views/CheckOut.fxml");

}

private void handleEditBookAction(ActionEvent event) throws IOException{
	
    loadFxml("Edit Book", "/Views/EditBookScreen.fxml");

}
}