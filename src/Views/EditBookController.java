package Views;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

import Controllers.BookCopyCtrl;
import Controllers.BookInfoCtrl;
import Data.StaticData;
import Models.Author;
import Models.BookCopy;
import Models.BookInfo;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;

public class EditBookController implements Initializable {
	  @FXML  Button btnLookup;
	  @FXML  TextField txtlookupisbn;
	  
	  @FXML  TextField txtisbn;
	  @FXML  TextField txttitle;
	  @FXML  TextField txtnocopies;
	  @FXML  ComboBox<String> cbauthors;
	  @FXML  Button btnupdate;

	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
		fillComboBoxAuthors();
		btnLookup.setOnAction(event -> {
			try {
				handleLookupkAction(event);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		});
		
		btnupdate.setOnAction(event -> {
			try {
				handlebtnUpdatekAction(event);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		});
		
	}
	
	private void handleLookupkAction(ActionEvent event) throws NumberFormatException, Exception
	{
		txtisbn.editableProperty().setValue(false);
		String isbn=txtlookupisbn.getText();
		
		BookInfo resBook= BookInfoCtrl.FindBookByISBN(isbn);
		
		if(resBook==null)
		{
			 Alert alert = new Alert(AlertType.ERROR, "ISBN not exist");
		 alert.show();
		 return;
		}
		
		txtisbn.setText(resBook.getISBN());
		txttitle.setText(resBook.getTitle());
		
		Author auther=resBook.getAuthors().get(0);
		cbauthors.setValue(auther.getFirstName());
		txtnocopies.setText(Integer.toString(resBook.getBookCopies().size()));	
		
	}
	
	private void handlebtnUpdatekAction(ActionEvent event) throws NumberFormatException, Exception{
		
		if(txtisbn.getText().equals("") || txtisbn.getText().equals(null))
		{
			 Alert alert = new Alert(AlertType.ERROR, "NO book Selected");
		     alert.show();
		      return;
		}
		
		String authorName=cbauthors.getValue();
		Author au=new Author();
		for(Author A:StaticData.authors) 
		{
			if(A.getFirstName().equals(authorName))
				au=A;
		}
	 ArrayList<Author> authors=new ArrayList<Author>();
	 authors.add(au);

	 ArrayList<BookCopy> bookcopies=new ArrayList<BookCopy>();	
	 
	 BookInfo UpdBook=new BookInfo(txtisbn.getText(),txttitle.getText(),authors,bookcopies);
	
	/* BookInfo UpdBook= BookInfoCtrl.AddBookInfo(txtisbn.getText(),txttitle.getText(),authors,bookcopies);
	 BookCopyCtrl.AddBookCopiesByISBN(txtisbn.getText(),Integer.parseInt(txtnocopies.getText()));	*/ 
	 
	 BookInfoCtrl.UpdateBook(UpdBook,Integer.parseInt(txtnocopies.getText()));
		
	 
      String str=StaticData.PrintBookInfo(UpdBook);
  	 

	 
	 Alert alert = new Alert(AlertType.INFORMATION, str);
	 alert.show();
	 
	 txtlookupisbn.setText("");
	 txttitle.setText("");
	 txtnocopies.setText("");
	 cbauthors.setValue("");
	 txtisbn.setText("");
	 
	 
	/*	// String _isbn,String _title, ArrayList<Author> _authors,ArrayList<BookCopy> _bookCopies
		
		String authorName=author.getValue();
		Author au=new Author();
		for(Author A:StaticData.authors) 
		{
			if(A.getFirstName().equals(authorName))
				au=A;
		}
	 ArrayList<Author> authors=new ArrayList<Author>();
	 authors.add(au);

	 ArrayList<BookCopy> bookcopies=new ArrayList<BookCopy>();	
	 
	 BookInfo newBook= BookInfoCtrl.AddBookInfo(isbn.getText(),name.getText(),authors,bookcopies);
	 BookCopyCtrl.AddBookCopiesByISBN(isbn.getText(),Integer.parseInt(copies.getText()));	 
	
	 // Set NewBook for author
	 BookInfoCtrl.AssigBookToAuthor(newBook, au);
	
	 
	 
	 
	 isbn.setText("");
	 name.setText("");
	 copies.setText("");
	 author.setValue("");
	 
	 
	 String str=StaticData.PrintBookInfo(newBook);
	 

	 
	 Alert alert = new Alert(AlertType.INFORMATION, str);
	 alert.show();*/
	}
	
	
	
	private void fillComboBoxAuthors()
	{
		for(Author a:StaticData.authors)
		{
			cbauthors.getItems().add(a.getFirstName());
		}
		
	}
}
