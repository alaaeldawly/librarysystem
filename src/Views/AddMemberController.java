package Views;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import Controllers.AdminCtrl;
import Models.User;
import Models.User.roleType;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class AddMemberController implements Initializable {
	  @FXML  TextField ID;
	  @FXML  TextField fname;
	  @FXML  TextField lname;
	  @FXML  TextField street;
	  @FXML  TextField zip;
	  @FXML  TextField city;
	  @FXML  TextField phone;

	  @FXML  Button addbtn;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
		addbtn.setOnAction(event -> {
			try {
				handleAddMemberAction(event);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		});
		
	}


	private void handleAddMemberAction(ActionEvent event) throws IOException{
		User member = new User(ID.getText(), "", fname.getText(), lname.getText());
		member.addRole(roleType.MEMBER);
		AdminCtrl.AddNewMember(member);
		new Alert(Alert.AlertType.INFORMATION, "Member Added!").showAndWait();	
		ID.setText("");
		fname.setText("");
		lname.setText("");
		street.setText("");
		zip.setText("");
		city.setText("");
		phone.setText("");
	}

}
